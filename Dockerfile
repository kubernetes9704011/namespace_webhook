FROM python:slim-buster
WORKDIR /app
COPY webhook.py /app/
RUN pip3 install \
  flask \
  requests \
  jsonify \
  kubernetes
CMD ["python", "webhook.py"]
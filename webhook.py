from flask import Flask, request, jsonify
from kubernetes import client, config
import json

app = Flask(__name__)

@app.route('/mutate', methods=['POST'])
def handle_mutate():
    admission_review_request = json.loads(request.data)
    namespace = admission_review_request['request']['namespace']

    config.load_incluster_config()
    v1 = client.CoreV1Api()

    try:
        v1.read_namespace(namespace)
    except client.exceptions.ApiException as e:
        if e.status == 404:
            ns_body = client.V1Namespace(metadata=client.V1ObjectMeta(name=namespace))
            v1.create_namespace(ns_body)

    response = {
        "apiVersion": "admission.k8s.io/v1",
        "kind": "AdmissionReview",
        "response": {
            "uid": admission_review_request['request']['uid'],
            "allowed": True
        }
    }
    return jsonify(response)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, ssl_context=('/etc/tls/tls.crt', '/etc/tls/tls.key'))